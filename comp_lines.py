#!/bin/env python

"""
Prints the list of lines resulted from the minus operation of 2 files' lines.
Basically this script executes this operation:
    print(comparable_file_lines - container_file_lines)

Usage:

./comp_lines.py <container_file> <comparable_file>

container_file: Name of the file (without the extension) which provides the 
lines against this script will check the other file's lines.
comparable_file: Name of the file (whithout the extension) which provides the 
lines to be checked are in the other file.

Please note the files should be under ./files/ directory, and should have the 
extension .txt
"""

import sys

def _get_lines(_file):
    """Return the list of lines of '_file', without break lines."""
    with open("./files/{0}.txt".format(_file)) as _f:
        return [entry.strip() for entry in _f.readlines()]

try:
    container_file = sys.argv[1]
    comparable_file = sys.argv[2]
    cont_list = _get_lines(container_file)
    comp_list = _get_lines(comparable_file)
    difflist = list(set(comp_list) - set(cont_list))
    liststr = "\n".join(sorted(difflist))
    msg = "{0}{1}".format("=" * 60, '\n')
    msg += "All lines from '{0}.txt' are in '{1}.txt'".format(comparable_file, 
                                                             container_file) \
            if not difflist \
            else ("These lines are in '{0}.txt', but not in '{1}.txt':\n").\
                format(comparable_file, container_file)
    msg += ("{0}{1}{2}".format("=" * 60, '\n', liststr))
    print(msg)
except Exception as e:
    print(str(e))
